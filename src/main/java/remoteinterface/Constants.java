package remoteinterface;

/**
 * {Insert class description here}
 *
 * 2015/11/17 
 * @author <a href="mailto:osre1300@student.miun.se">Oscar Reimer</a><br><a href="mailto:mabo0920@student.miun.se">Martin Book</a>
 */

public class Constants 
{
    public static final String RMI_ID = "osre1300_mabo0920";
    public static final int RMI_PORT = 1100;
}
