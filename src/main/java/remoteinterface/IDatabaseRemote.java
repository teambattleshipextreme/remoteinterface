
package remoteinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * {Insert interface description here}
 * 
 * 2015/11/17 
 * @author <a href="mailto:osre1300@student.miun.se">Oscar Reimer</a><br><a href="mailto:mabo0920@student.miun.se">Martin Book</a>
 */
public interface IDatabaseRemote extends Remote
{
    public boolean createInitialDatabase(String path) throws RemoteException;
}
